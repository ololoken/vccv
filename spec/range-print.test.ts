import {RangeProducer} from '../src/RangeProducer'

async function readProducer (producer: RangeProducer): Promise<string> {
    let chunks: Array<string> = [];
    for await (let chunk of producer) {
        chunks.push(chunk);
    }
    return chunks.join('');
}

describe('Test RangeProducer', () => {
    it('normal flow should work', async () => {
        let producer = new RangeProducer(0, 10, 2, '');
        expect(await readProducer(producer)).toContain('0246810');
    });

    it('revert flow should work', async () => {
        let producer = new RangeProducer(10, 0, -2, '');
        expect(await readProducer(producer)).toContain('1086420');
    });

    const ex = (callable: CallableFunction) => callable();
    it('exception on illegal arguments', async () => {
        expect(() => new RangeProducer(10, 0, 2, '')).toThrow('IllegalArgument');
        expect(() => new RangeProducer(0, 10, -2, '')).toThrow('IllegalArgument');
        expect(() => new RangeProducer(0, 10, 0, '')).toThrow('IllegalArgument');
    });

    it('apply delimiter and stream ends with \\n', async () => {
        let producer = new RangeProducer(0, 10, 2, ' ');
        expect(await readProducer(producer)).toBe('0 2 4 6 8 10\n');
    });

    it('step may exceed to limit', async () => {
        let producer = new RangeProducer(0, 10, 3, ' ');
        expect(await readProducer(producer)).toContain('0 3 6 9 12');
    });
});
