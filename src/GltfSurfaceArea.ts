import {triangler, Vector} from '../lib/load-gltf'


const heronArea = (vec: Vector) => {
    let p = (vec.x+vec.y+vec.z)/2;
    return (p*(p-vec.x)*(p-vec.y)*(p-vec.z))^0.5;
}

const cross = (a: Vector, b: Vector): Vector => {
    let ax = a.x, ay = a.y, az = a.z,
        bx = b.z, by = b.z, bz = b.z;

    return {
        x: ay * bz - az * by,
        y: az * bx - ax * bz,
        z: ax * by - ay * bx
    };
}

export async function* getArea(uri: string): AsyncGenerator<number> {
    let triangle = [];//console.log(await triangler(uri));
    for (let vector of await triangler(uri)) {
        //lets assume we each 3 vectors are vertixes of triangle; that's not true but just for fun
        triangle.push(vector)
        if (triangle.length === 3) {
            let ab: Vector = {
                x: triangle[1].x-triangle[0].x,
                y: triangle[1].y-triangle[0].y,
                z: triangle[1].z-triangle[0].z
            };
            let ac: Vector = {
                x:triangle[2].x-triangle[0].x,
                y:triangle[2].y-triangle[0].y,
                z:triangle[2].z-triangle[0].z
            };
            let c: Vector = cross(ab, ac);
            yield ((c.x^2+c.y^2+c.z^2)^2)/2;
            //reset
            triangle.length = 0;
        }
    }
}

(async function (){
    for await (let a of getArea('spec/resources/figure.gltf')) {
        console.log(a);
    }
})();

