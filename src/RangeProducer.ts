import { Readable } from 'stream'

export class RangeProducer extends Readable {
    private counter: number;
    private readonly from: number;
    private readonly to: number;
    private readonly step: number;
    private readonly delim: string;

    constructor(from: number, to: number, step: number, delim: string) {
        super();
        if ((from > to && step >= 0) || (from < to && step <= 0)) throw new Error('IllegalArgument');
        this.counter = from;
        this.from = from;
        this.to = to;
        this.step = step;
        this.delim = delim;
    }

    _read(): void {
        this.push(`${this.counter}${this.delim}`);
        this.counter+=this.step;
        if (this.from < this.to && this.counter >= this.to) {
            this.push(`${this.counter}\n`);
            this.push(null);
        }
        if (this.from > this.to && this.counter <= this.to) {
            this.push(`${this.counter}\n`);
            this.push(null);
        }
    }

}
