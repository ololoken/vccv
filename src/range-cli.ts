import yargs, {Argv} from 'yargs';
import {RangeProducer} from "./RangeProducer";

let argv = yargs(process.argv.slice(2))
    .options({
        from: {alias: 'f', type: 'number', default: 0, demandOption: true},
        to: {alias: 'to', type: 'number', demandOption: true},
        step: {alias: 's', type: 'number', default: 1, demandOption: false},
        delim: {alias: 'd', type: 'string', default: ' ', demandOption: true}
    })
    .usage('$0 <to>', 'print range of numbers from',
        (y: Argv) => y.positional('to', {description: 'last range value'}))
    .help().argv;

new RangeProducer(argv.from, argv.to, argv.step, argv.delim).pipe(process.stdout);
