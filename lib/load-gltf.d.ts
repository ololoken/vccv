export interface Vector {
    x: number,
    y: number,
    z: number
}
export function triangler (uri: string): AsyncGenerator<Vector>
