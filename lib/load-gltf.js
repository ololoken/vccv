const fs = require('fs');
//a portion of black magic
global.THREE = require('three');
global.atob = (a) => Buffer.from(a, 'base64').toString('binary');
require(`../node_modules/three/examples/js/loaders/GLTFLoader.js`)

//new THREE.GLTFLoader()

const triangler = (uri) => {
    let buffer = fs.readFileSync(uri, 'binary')
    return new Promise((resolve, reject) => new THREE.GLTFLoader().parse(buffer, buffer, (gltf) => {
        //console.log(gltf.scene.children[2].geometry.attributes.position);
        //console.log(gltf.scene.children[2].geometry.index);
        resolve((function* ()  {
            for (let child of gltf.scene.children) {
                for (let child of gltf.scene.children) {
                    if (child.type === 'Mesh') {
                        for (let i = 0; i < child.geometry.index.count; i += 3) {
                            yield {
                                x: child.geometry.attributes.position.array[child.geometry.index.array[i]],
                                y: child.geometry.attributes.position.array[child.geometry.index.array[i+1]],
                                z: child.geometry.attributes.position.array[child.geometry.index.array[i+2]],
                            };
                        }
                    }
                }
            }
        })());
    }, () => {}, (err) => reject(err)));
}

module.exports = {triangler};

